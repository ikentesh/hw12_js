"use strict";

/*Теоретичні питання

Чому для роботи з input не рекомендується використовувати клавіатуру?

Тому що KeyboardEvent повідомить лише про те, що на клавіші сталася подія. 
Та якщо користувач вводить текст рукописним способом, припустимо з планшета, 
події клавіш можуть не виникати.*/

// Завдання

const button = document.querySelectorAll(".btn");
const arrayOfButtons = [...button];

document.body.addEventListener("keypress", (event) => {
  arrayOfButtons.forEach((element) => {
    let eventCodeValue;
    element.style.backgroundColor = "black";
    if (event.code.includes("Key")) {
      eventCodeValue = event.code.slice(3);
    } else {
      eventCodeValue = event.code;
    }
    if (eventCodeValue === element.innerText) {
      element.style.backgroundColor = "blue";
    }
  });
});
